import { HttpClient } from '@angular/common/http';
import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { from, Observable, of } from 'rxjs';
import { AppComponent } from './app.component';
import { TestServicesService } from './test-services.service';

describe('AppComponent', () => {
  const serviceSpy= {
    get: () => { return {subscribe: () => {}}; },
  };
  beforeEach(async(async () => {
   await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers:[
        // {provide: HttpClient ,useClass:jasmine.createSpyObj('HttpClient')  },
        {provide: TestServicesService ,useValue:serviceSpy  }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angualar-test'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('angualar-test');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('angualar-test app is running!');
  });
  it('should render res value', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
   
   spyOn(serviceSpy, 'get').and.returnValue( of(1));
   fixture.detectChanges();
   fixture.whenStable().then(() => {
        expect(app.res).toEqual(1);
      });
   //doneIn();
  });
});


