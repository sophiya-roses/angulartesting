import { Component, OnInit } from '@angular/core';
import { TestServicesService } from './test-services.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'angualar-test';
  res:Number|null=null;

  constructor(private t:TestServicesService) { }
  ngOnInit(): void {
  this.t.get().subscribe(dara=>{
   this.res=dara;
   });
  }

}
