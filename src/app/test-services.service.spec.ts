import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http'
import { TestServicesService } from './test-services.service';

describe('TestServicesService', () => {
  let service: TestServicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers:[
        // {provide: HttpClient ,useClass:jasmine.createSpyObj('HttpClient')  }
      ]
    });
    service = TestBed.inject(TestServicesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  
  it('check if 34', (doneIn:DoneFn) => {
    service.get().subscribe(res=>{
      expect(res). toBe(34);
      doneIn();
    })
   
  });
});
