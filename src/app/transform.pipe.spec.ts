import { TransformPipe } from './transform.pipe';

describe('TransformPipe', () => {
  it('create an instance', () => {
    const pipe = new TransformPipe();
    expect(pipe).toBeTruthy();
  });

  it('check pipe', () => {
    const pipe = new TransformPipe();
    expect(pipe.transform('forr')).toBe('FORR');
  });

});
